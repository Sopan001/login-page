import styles from "./Header.module.scss";
import logo from "../../assets/images/bookingplanner_logo.png";
const Header = () =>{
    return(
        <header className={styles.header}>
            <img src={logo} alt="Logo"/>
            {/* <h1><span>b</span>ookingplanner</h1> */}
        </header>
    )
}

export default Header;