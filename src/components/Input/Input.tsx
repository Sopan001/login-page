import styles from "./Input.module.scss";
import { IProps } from "./Input.types";

const Input = ({
  name,
  placeholder,
  type,
  label,
  register,
  errors,
}: IProps) => {

  return (
    <div className={styles.inputBox}>
      <span>{label}</span>
      <input
        type={type}
        placeholder={placeholder}
        {...register(name)}
        autoComplete="off"
      />

      <p className={styles.error}>{errors[name] ? errors[name].message : null}</p>
    </div>
  );
};

export default Input;
