export interface IProps{
    type:string;
    placeholder:string;
    label:string;
    name:string;
    register:any;
    errors:any;
}