import styles from "./Button.module.scss";

const Button = () => {
    return(
        <button type="submit" className={styles.btn}>Login</button>
    )
}

export default Button;