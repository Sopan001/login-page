import styles from "./Footer.module.scss";

const Footer = () => {
    return(
        <footer className={styles.footer}>
            <span>@2021 All Rights Reserved</span>
        </footer>
    )
}

export default Footer;