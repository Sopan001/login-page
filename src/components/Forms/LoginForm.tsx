import Button from "../Button/Button";
import Input from "../Input/Input";
import styles from "./Forms.module.scss";
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { ILogin } from "./Login.types";

const schema = yup.object().shape({
    username: yup.string().required("*Email ID is required").matches(/^[\w.+/-]+@gmail\.com$/,"*Invalid Email-ID"),
    password: yup.string().required("*Password is required").min(5,"*Password must be at least 5 characters"),
  }).required();


const LoginForm = () => {

    const { register, handleSubmit,formState:{ errors } } = useForm<ILogin>({
        resolver: yupResolver(schema),
      });

    const onSubmit = (data:ILogin) => {
        console.log(data)
    }

  return (
    <form className={styles.container} onSubmit={handleSubmit(onSubmit)}>
      <h2>Log in to Bookingplanner</h2>

      <Input
        name="username"
        type="email"
        placeholder="name@gmail.com"
        label="Email Address"
        register={register}
        errors={errors}
      />

      <Input
        name="password"
        type="password"
        placeholder="Password"
        label="Password"
        register={register}
        errors={errors}
      />

      <div className={styles.row}>
        <div>
          <input type="checkbox" />
          Remember me
        </div>
        <a href="/">Forgot Your Password?</a>
      </div>

      <Button />
    </form>
  );
};

export default LoginForm;
