import Footer from "../../components/Footer/Footer";
import LoginForm from "../../components/Forms/LoginForm";
import Header from "../../components/Header/Header";
import styles from "./Login.module.scss";

const Login = () => {
    return(
        <section className={styles.container}>
            <div className={styles.bg}>
            </div>

            <Header/>
            <LoginForm/>
            <Footer/>
        </section>
    )
}

export default Login;